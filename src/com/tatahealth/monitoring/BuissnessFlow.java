package com.tatahealth.monitoring;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;


@Listeners(TestListner.class)
public class BuissnessFlow {

	
		
	

		public static WebDriver driver;;

		public WebDriverWait wait = null;
		public String webURL = "http://tatahealthplus.com/";
		

		@BeforeClass
		public void setUp() {
			// String chromedrvpath="E:\\workspace\\webautomationrepo\\chromedriver_win32_2.1\\chromedriver.exe";
			// String Username = System.getProperty("user.name");
		  //   final String userProfile= "C:\\Users\\"+Username+"\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\";
		/*	 ChromeOptions options = new ChromeOptions();
		// options.addArguments("user-data-dir="+userProfile);
		 options.addArguments("--start-maximized");
			
			System.setProperty("webdriver.chrome.driver",chromedrvpath);
			driver = new ChromeDriver(options);*/
			 
				DesiredCapabilities caps = new DesiredCapabilities();
				caps.setJavascriptEnabled(true);                       
			
				
				//	caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "E:\\workspace\\webautomationrepo\\libs\\phantomjs-1.9.7-windows\\phantomjs.exe");
				
				driver = new HtmlUnitDriver(caps);
		/*	FirefoxProfile profile = new FirefoxProfile();
			profile.setPreference("browser.usedOnWindows10", false);
			profile.setPreference("browser.usedOnWindows10.introURL", "about:blank");
			driver=new FirefoxDriver(profile);*/
			driver.manage().window().maximize();
			wait = new WebDriverWait(driver, 30);
			
		}

		@Test(priority = 1)
		public void is_TATAHealthPlus_Home_Page_Up() {
			System.out.println("First Testcase");
			driver.get(webURL);
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("forgotPassForm")));
			String title=driver.getTitle();
			System.out.println("title  "+title);
			Assert.assertTrue(driver.getTitle().equalsIgnoreCase("TATA-DHP"));
		}



		@Test(priority = 2)
		public void is_User_is_able_to_log_In() throws Exception {
			System.out.println("second Testcase");
			try {
				//driver.findElement(By.xpath("//div/label[2]")).click();
				Thread.sleep(2000);
			driver.findElement(By.id("txtUsername")).sendKeys("UISYNTHETIC");
			driver.findElement(By.id("txtPassword")).sendKeys("Test@123");
			driver.findElement(By.id("login")).click();
			try{
				Thread.sleep(4000);
				driver.findElement(By.xpath("//div[@class='sa-confirm-button-container']/button")).click();
			}catch(Exception e){
				System.out.println(" inside catch block");
			}
	Thread.sleep(7000);
try{
			//WebDriverWait wait = new WebDriverWait(driver, 30);
		//	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='chooseClinicAnchor']/div[contains(text(),'Tata Health Plus')]")));
			WebElement tataPlus=driver.findElement(By.xpath("//a[@class='chooseClinicAnchor']/div[contains(text(),'Tata Health Plus')]"));
		Assert.assertTrue(tataPlus.isDisplayed());
          tataPlus.click();
}catch(Exception e){
				System.out.println(" inside catch block");
			}
		WebDriverWait wait1 = new WebDriverWait(driver, 30);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("userProfileName")));
		Assert.assertTrue(driver.findElement(By.id("userProfileName")).isDisplayed());
		System.out.println("llogged in");
		
				
			} catch (Exception e) {
				throw new Exception("User is not able to login, as Profile pic logo is not displayed on the Home page after Entering Username and password");
			}
		}
		
	
		
		@AfterClass
		public void tearDown() {
			driver.quit();
		}
		
		


}

