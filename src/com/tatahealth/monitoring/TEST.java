package com.tatahealth.monitoring;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TEST {
	public static WebDriver driver;;

	public WebDriverWait wait = null;
	public String webURL = "http://173.244.201.160:2081/";
	

	@BeforeClass
	public void setUp() {
		 String chromedrvpath="E:\\tata_Backup\\Ganesh\\Automation\\webautomationrepo\\chromedriver_win32_2.1\\chromedriver.exe";
		// String Username = System.getProperty("user.name");
	  //   final String userProfile= "C:\\Users\\"+Username+"\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\";
		 ChromeOptions options = new ChromeOptions();
	// options.addArguments("user-data-dir="+userProfile);
	 options.addArguments("--start-maximized");
		
		System.setProperty("webdriver.chrome.driver",chromedrvpath);
		driver = new ChromeDriver(options);
	/*	FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.usedOnWindows10", false);
		profile.setPreference("browser.usedOnWindows10.introURL", "about:blank");
		driver=new FirefoxDriver(profile);*/
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 20);
		
	}

	@Test(priority = 1)
	public void is_TATAHealthPlus_Home_Page_Up() {
		System.out.println("First Testcase");
		driver.get(webURL);
		//wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("forgotPassForm")));
		String title=driver.getTitle();
		System.out.println("title  "+title);
	//	Assert.assertTrue(driver.getTitle().equalsIgnoreCase("TATA-DHP"));
	}



	

	
	@AfterClass
	public void tearDown() {
		driver.quit();
	}
	
}
