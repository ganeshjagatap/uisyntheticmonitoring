package ClinicUISyntheticMonitoring.ClinicUISyntheticMonitoring1;



import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;





public class TestListner implements ITestListener {

	
	public void onTestStart(ITestResult result) {
		System.out.println("UseCase :" + getTestMethodName(result) + " [Start]");
	}

	public void onTestSuccess(ITestResult result) {
		System.out.println("UseCase :" + getTestMethodName(result) + " [success]");
		//SendMailTLS.SuccessMailsend("[Build Success]: "+getTestMethodName(result));
	}


	public void onTestFailure(ITestResult result) {
		//File scrFile = ((TakesScreenshot)BuissnessFlow.driver).getScreenshotAs(OutputType.FILE);

		try {
			//FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "/ReportUserFlow/screenshot.png"));
			Thread.sleep(2000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// getting the size
		//new File(System.getProperty("user.dir") + "/ReportUserFlow/screenshot.png");
		String FailureMessage = result.getThrowable().getMessage();
		SendMailTLS.FailureMailsend("[Build Failure]: "+getTestMethodName(result),FailureMessage);

		System.out.println("UseCase :" + getTestMethodName(result) + " [Failure]");
	}

	
	public void onTestSkipped(ITestResult result) {
		System.out.println("UseCase :" + getTestMethodName(result) + " [Skipped]");
	}

	
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		System.out.println("test failed but within success % " + getTestMethodName(result));
	}

	
	public void onStart(ITestContext context) {
		//System.out.println("#############on start of test " + context.getName());
	}


	public void onFinish(ITestContext context) {
		//System.out.println("#############on finish of test " + context.getName());
	}

	private static String getTestMethodName(ITestResult result) {
		return result.getMethod().getConstructorOrMethod().getName();
	}
}