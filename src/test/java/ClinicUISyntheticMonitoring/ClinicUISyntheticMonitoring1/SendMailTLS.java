package ClinicUISyntheticMonitoring.ClinicUISyntheticMonitoring1;



import java.awt.image.BufferedImage;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Properties;










import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.openqa.selenium.remote.DesiredCapabilities;

public class SendMailTLS {
	 public static String html_Text;
	  Properties emailProperties;
	  Session mailSession;
	  MimeMessage emailMessage;
	  BufferedImage chartimg;
	  DesiredCapabilities caps;
	
    //Send the Daily Mail Report to the People 
	public static void Mailsend(String toalTESTrUNcount, String passcount, String failcount, String averageloadingtime) {
		final String username = "ganesh.jagatap@tatahealth.com";
		final String password = "tata@1234";
		
		// Mention the CC and To in the Array Format
		String[] to = { "ganesh.jagatap@tatahealth.com" };

		String[] cc = { "ganesh.jagatap@tatahealth.com" };

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("ganeshtechnical.24@gmail.com"));

			for (int i = 0; i < to.length; i++) {

				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to[i]));

			}

			for (int i = 0; i < cc.length; i++) {

				message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc[i]));

			}
			message.setSubject("Test Report for TATA Digital Home Page ");
			String text = "TATA Digital Home Page Ping Test Report for Last 24 Hours,\n\n Total" + " Tests Run =   " + toalTESTrUNcount + "\n\n Total Tests Passed = " + passcount + "\n\n Total " + "Tests Failed  = " + failcount
					+ "\n\n Average loading time for the site = " + averageloadingtime + " Seconds";
			message.setText(text);

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
   //Success Send
	
	public static void SuccessMailsend(String subject) {
		 final String username = "ganesh.jagatap@tatahealth.com";
		final String password = "tata@1234";

		//String[] to = { "qa@TATA Digital.com" };

		//String[] cc = { "ganeshtechnical.24@gmail.com", };

		String[] to = { "ganesh.jagatap@tata-dhp.com" };

		String[] cc = { "ganeshtechnical.24@gmail.com" };

		
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("TataHealthAutomation"));

			for (int i = 0; i < to.length; i++) {

				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to[i]));

			}

			for (int i = 0; i < cc.length; i++) {

				message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc[i]));

			}
			Date date= new Date();
			System.out.println("date "+date);
			Timestamp dateCurrent= new Timestamp(date.getTime());
			// Set the Subject with time stamp
			message.setSubject(subject+" "+ dateCurrent);
			//String text = "TATA Digital home page download timeout exceeded threshold of 20 seconds.\n\n Regards,\nAutomation Team";
			//String snapshotpath = System.getProperty("user.dir") + snapshot;
		//	String snapshotFilename = "screenshot.png";
		//	message.setText(text);

			BodyPart messageBodyPart = new MimeBodyPart();
			String htmlText = "<H4 style=" + "color:green;" + ">"+subject+" Usecase Success at : "+dateCurrent+">";
			messageBodyPart.setContent(htmlText, "text/html");

			// Create a related multi-part to combine the parts
			MimeMultipart multipart = new MimeMultipart("related");
			multipart.addBodyPart(messageBodyPart);

			// Create part for the image
			messageBodyPart = new MimeBodyPart();

			// Fetch the image and associate to part
		//	FileDataSource fds = new FileDataSource(snapshotpath);
			//messageBodyPart.setDataHandler(new DataHandler(fds));
			messageBodyPart.setHeader("Content-ID", "<memememe>");

			// Add part to multi-part
			multipart.addBodyPart(messageBodyPart);
			// Associate multi-part with message
			message.setContent(multipart);

			message.saveChanges();

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}}
	// Send the Failure Report
	public static void FailureMailsend(String subject,String causeMessage) {
		 final String username = "ganesh.jagatap@tatahealth.com";
		final String password = "tata@1234";

		String[] to = { "ganesh.jagatap@tatahealth.com" };

		String[] cc = { "ganesh.jagatap@tatahealth.com" };

		
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("ganesh.jagatap@tata-dhp.com"));

			for (int i = 0; i < to.length; i++) {

				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to[i]));

			}

			for (int i = 0; i < cc.length; i++) {

				message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc[i]));

			}
			java.util.Date date= new java.util.Date();
			Timestamp dateCurrent= new Timestamp(date.getTime());
			// Set the Subject with time stamp
			message.setSubject(subject+" "+ dateCurrent);
		

			message.setSubject("Clinics UI Monitoring On Production [synthetic monitoring]");
			String text =  causeMessage;
			message.setText(text);

			Transport.send(message);

			System.out.println("Done");

		//	System.out.println("Done")
		//	System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args) {
		 SendMailTLS.Mailsend("231","3213","3123","3123.21");
		//SendMailTLS.FailureMailsend();
	}

}
